package edu.io;

import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.util.ArrayList;
import java.util.List;

import edu.io.addCosmetic.AddToDbActivity;
import edu.io.persistance.objects.Ingredient;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    private FirebaseFirestore db ;
    private List<Ingredient> ingredients =  new ArrayList<>();



    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }
    private AddToDbActivity addToDbActivity;

    @BeforeAll
    static void init(){ System.out.println("Zaczynamy testy!"); }

    @BeforeEach
    void setup(){
        System.out.println("Zaraz uruchomimy kolejny test ...");
    }



    @Test
    public void test() {


        boolean exists = false;
        String cosmeticIngridientTest = "Aqua";
        if(cosmeticIngridientTest.length() > 0) {
            Ingredient ingredient = new Ingredient();
            for(Ingredient ing : ingredients){
                if (cosmeticIngridientTest.equals(ing.getName())){
                    ingredient = ing;
                    exists = true;
                    break;
                }
                ingredient = new Ingredient(cosmeticIngridientTest, "unknown", "detail");
            }
        }
        assertTrue(exists);
    }




}