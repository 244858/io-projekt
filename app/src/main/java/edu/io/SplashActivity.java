package edu.io;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

import edu.io.scanner.ScannerActivity;

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Start home activity
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        boolean logged = mAuth.getCurrentUser() != null && mAuth.getCurrentUser().isEmailVerified();

        if(logged){
                startActivity(new Intent(SplashActivity.this, ScannerActivity.class));
                // close splash activity
                finish();
            } else {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                // close splash activity
                finish();
            }
        }
    }
