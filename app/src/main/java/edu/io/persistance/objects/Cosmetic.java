package edu.io.persistance.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Cosmetic implements Serializable {

    private Long id;
    private String name;
    private String producer;
    private List<String> purpose;
    private List<Ingredient> ingredients;
    private String howToUse;

    public Cosmetic() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Cosmetic(Long id, String name, String producer, List<String> purpose, List<Ingredient> ingredients, String howToUse) {
        this.id = id;
        this.name = name;
        this.producer = producer;
        this.purpose = purpose;
        this.ingredients = ingredients;
        this.howToUse = howToUse;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getProducer() {
        return producer;
    }

    public List<String> getPurpose() {
        return purpose;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public String getHowToUse() {
        return howToUse;
    }

    public void setHowToUse(String howToUse) {
        this.howToUse = howToUse;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public void setPurpose(List<String> purpose) {
        this.purpose = purpose;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {
        return name;
    }

    public String info() {
        return "Cosmetic{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", producer='" + producer + '\'' +
                ", purpose=" + purpose +
                ", ingredients=" + ingredients +
                ", howToUse='" + howToUse + '\'' +
                '}';
    }
}

