package edu.io.persistance.objects;

import java.io.Serializable;

public class Ingredient implements Serializable {
    private String name;
    private String harmfulness;
    private String detail;

    public Ingredient() {
    }

    public Ingredient(String name, String harmfulness, String detail) {
        this.name = name;
        this.harmfulness = harmfulness;
        this.detail = detail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHarmfulness() {
        return harmfulness;
    }

    public void setHarmfulness(String harmfulness) {
        this.harmfulness = harmfulness;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return name;
    }
}
