package edu.io;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

import edu.io.cosmeticInfo.CosmeticInfoActivity;

import static edu.io.IngredientsInfoActivity.INGREDIENTS_COLLECTION;

public class RegisterActivity extends AppCompatActivity {

    EditText userLogin, userPassword, userEmail;
    Button registerBtn;
    FirebaseAuth firebaseAuth;
    ProgressBar progressBar;
    TextView logInText;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        userLogin = findViewById(R.id.name);
        userPassword = findViewById(R.id.password);
        userEmail = findViewById(R.id.email);
        registerBtn = findViewById(R.id.register);

        firebaseAuth = FirebaseAuth.getInstance();
        progressBar = findViewById(R.id.progressBar2);

        registerBtn.setOnClickListener(view -> {
            String email = userEmail.getText().toString().trim();
            String password = userPassword.getText().toString().trim();

            if (TextUtils.isEmpty(email)) {
                userEmail.setError("Email is required.");
                return;
            }
            if (TextUtils.isEmpty(password)) {
                userPassword.setError("Password is required.");
                return;
            }

            if (password.length() < 6) {
                userPassword.setError("Password must be more than  or equals 6 characters. ");
                return;
            }

            progressBar.setVisibility(View.VISIBLE);

            // register the user in firebase

            firebaseAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {

                                firebaseAuth.getCurrentUser().sendEmailVerification()
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Toast.makeText(RegisterActivity.this, "User created", Toast.LENGTH_SHORT).show();
                                                    Customer customer = new Customer(email, 0, new ArrayList<>());
                                                    CollectionReference mColRef = db.collection("customers");
                                                    mColRef.document(email).set(customer)
                                                            .addOnCompleteListener(task2 -> {
                                                                if (task.isSuccessful()) {
                                                                    Log.d("***CUSTOMER ADDED***", "Document has been saved");
                                                                }
                                                            }).addOnFailureListener(e -> Log.w("***FAILED ADD***", "Document was not saved", e));


                                                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                                    Toast.makeText(RegisterActivity.this, "Please check your email to verify account", Toast.LENGTH_LONG).show();
                                                } else {
                                                    progressBar.setVisibility(View.GONE);
                                                    Toast.makeText(RegisterActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });


                            } else {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(RegisterActivity.this, "Error !" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        });
    }

    //hide keyboard when  touched outside editText
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyboard(this);
        }
        return super.dispatchTouchEvent(ev);
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }
}
