package edu.io;


import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import edu.io.cosmeticInfo.CosmeticInfoActivity;
import edu.io.persistance.objects.Cosmetic;

public class HowToUseActivity extends AppCompatActivity {

    TextView howToUse;
    Cosmetic cosmetic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_use);
        Bundle extras = getIntent().getExtras();
         cosmetic = (Cosmetic) extras.get("COSMETIC");
        String howToUseToSet = cosmetic.getHowToUse();
        howToUse = findViewById(R.id.howToUse);
        howToUse.setText(howToUseToSet);
    }

    public void onClick (View view){
        if (view.getId() == R.id.backBtnFloat) {
            Intent intent = new Intent(this, CosmeticInfoActivity.class);
            intent.putExtra("COSMETIC", cosmetic);
            startActivity(intent);
        }
    }
}
