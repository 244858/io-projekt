package edu.io;

import java.util.List;

import edu.io.persistance.objects.Cosmetic;

public class Customer {
    private String email;
    private int score;
    private List<Cosmetic> cosmetics;

    public Customer(String email, int score, List<Cosmetic> cosmetics) {
        this.email = email;
        this.score = score;
        this.cosmetics = cosmetics;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public List<Cosmetic> getCosmetics() {
        return cosmetics;
    }

    public void setCosmetics(List<Cosmetic> cosmetics) {
        this.cosmetics = cosmetics;
    }
}
