package edu.io;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import java.util.ArrayList;
import java.util.Objects;
import edu.io.cosmeticInfo.CosmeticInfoActivity;
import edu.io.persistance.objects.Cosmetic;
import edu.io.scanner.ScannerActivity;

public class AccountActivity extends AppCompatActivity {

    TextView points;
    ListView userCosmetics;

    FirebaseAuth firebaseAuth;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    ArrayList<Cosmetic> userCosmeticList = new ArrayList<>();
    double score = 0 ;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        firebaseAuth = FirebaseAuth.getInstance();
        points = findViewById(R.id.pointsResult);
        userCosmetics = findViewById(R.id.userCosmeticList);
        String userEmail = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getEmail();
        CollectionReference colRef = db.collection("customers");

        assert userEmail != null;
        colRef.document(userEmail).get().addOnCompleteListener(task1 -> {
            if (task1.isSuccessful()) {
                DocumentSnapshot document = task1.getResult();
                if (document.exists()) {
                    score =  document.getDouble("score");
                    String scoreStr = String.valueOf((int)score);
                    points.setText(scoreStr);

                    db.collection("customers").document(userEmail).collection("cosmetics").get().addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document2 : task.getResult()) {
                                Cosmetic cosmetic = document2.toObject(Cosmetic.class);
                                userCosmeticList.add(cosmetic);
                            }
                            ArrayAdapter<Cosmetic> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, userCosmeticList);
                            userCosmetics.setAdapter(adapter);
                        }
                    });

                } else {
                    Log.d("***NO DATA***", "No such document");
                }
            } else {
                Log.d("***FAILED***", "get failed with ", task1.getException());
            }
        });

        userCosmetics.setOnItemClickListener((parent, view, position, id) -> {
            Cosmetic cosmetic = userCosmeticList.get(position);
            Intent intent = new Intent(this, CosmeticInfoActivity.class);
            intent.putExtra("COSMETIC", cosmetic);
            startActivity(intent);
        });
    }

    public void onClick(View v) {
        if (v.getId() == R.id.backBtnFloat2) {
            Intent intent = new Intent(this, ScannerActivity.class);
            startActivity(intent);
        }
    }
}
