package edu.io.cosmeticInfo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import edu.io.AfterScan;
import edu.io.HowToUseActivity;
import edu.io.IngredientsInfoActivity;
import edu.io.LogInActivity;
import edu.io.MainActivity;
import edu.io.R;
import edu.io.persistance.objects.Cosmetic;
import edu.io.scanner.ScannerActivity;

public class CosmeticInfoActivity extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    TextView cosmeticName;
    TextView cosmeticProducer;
    TextView cosmeticPurpose;
    Cosmetic cosmetic;
    FirebaseAuth firebaseAuth;
    Button addDelete;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_cosmetic2);

        cosmeticName = findViewById(R.id.nameTextView2);
        cosmeticProducer = findViewById(R.id.producerTextView2);
        cosmeticPurpose = findViewById(R.id.purposeTextView2);
        addDelete = findViewById(R.id.addToYourCosmetics);

        firebaseAuth = FirebaseAuth.getInstance();

        Bundle extras = getIntent().getExtras();
        cosmetic = (Cosmetic) extras.get(AfterScan.COSMETIC);

        String name = cosmetic.getName();
        String producer = cosmetic.getProducer();
        List<String> purpose = cosmetic.getPurpose();

        cosmeticName.setText(name);
        cosmeticProducer.setText(producer);
        cosmeticPurpose.setText(purpose.toString());
        String email = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getEmail();

        CollectionReference colRef = db.collection("customers");
        CollectionReference cosmeticsRef = colRef.document(email).collection("cosmetics");
        DocumentReference docRef =  cosmeticsRef.document(cosmetic.getId().toString());
        docRef.get().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                DocumentSnapshot documentSnapshot = task.getResult();
                if(documentSnapshot.exists()){
                    addDelete.setText("Remove from your cosmetics");
                    addDelete.setOnClickListener(l -> {
                        docRef.delete();
                        Toast.makeText(CosmeticInfoActivity.this, "Cosmetic deleted from your collection", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(this, ScannerActivity.class);
                        startActivity(intent);
                    });

                }else{
                    addDelete.setText("Add to your cosmetics");
                    addDelete.setOnClickListener(l -> {
                        docRef.set(cosmetic);
                        Toast.makeText(CosmeticInfoActivity.this, "Cosmetic added to your collection", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(this, ScannerActivity.class);
                        startActivity(intent);
                    });
                }
            }
        });
    }

    public void ingredientsInfoClicked(View view) {
        Intent intent = new Intent(this, IngredientsInfoActivity.class);
        intent.putExtra("COSMETIC", cosmetic);
        startActivity(intent);
    }

    public void howToUseClicked(View view) {
        Intent intent = new Intent(this, HowToUseActivity.class);
        intent.putExtra("COSMETIC", cosmetic);
        startActivity(intent);
    }

    public void backClicked(View view){
        Intent intent = new Intent(this, ScannerActivity.class);
        startActivity(intent);
    }
    @SuppressLint("MissingSuperCall")
    @Override
    public void onBackPressed()
    {
        //do nothing when try go back
    }
}





