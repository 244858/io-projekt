package edu.io.cosmeticInfo;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import edu.io.R;
import edu.io.persistance.objects.Ingredient;

public class IngredientInfoAdapter extends BaseAdapter {


    private Context context;
    private List<Ingredient> ingredients;
    private ListView ingredientsListView;
    private boolean healthy;

    public IngredientInfoAdapter(Context context, List<Ingredient> ingredients, ListView ingredientsListView, boolean healthy) {
        this.context = context;
        this.ingredients = ingredients;
        this.ingredientsListView = ingredientsListView;
        this.healthy = healthy;
    }

    @Override
    public int getCount() {
        return ingredients.size();
    }

    @Override
    public Object getItem(int position) {
        return ingredients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // reuse views
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View rowView = inflater.inflate(R.layout.harmful_list_row, null);
        TextView nameTextView = (TextView) rowView.findViewById(R.id.rowTextView);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.rowImageView);

        if(!healthy) {
            imageView.setImageResource(R.drawable.nok_icon);
        }else{
            imageView.setImageResource(R.drawable.ok_icon);
        }
        imageView.setMaxHeight(24);
        imageView.setMaxWidth(24);
        nameTextView.setText(ingredients.get(position).getName());

        return rowView;
    }
}
