package edu.io.scanner;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;

import com.google.android.gms.vision.barcode.Barcode;
import com.google.firebase.auth.FirebaseAuth;

import edu.io.AccountActivity;
import edu.io.AfterScan;
import edu.io.LogInActivity;
import edu.io.MainActivity;
import edu.io.R;
import edu.io.SearchActivity;
import edu.io.cosmeticInfo.CosmeticInfoActivity;

/**
 * Scanner activity for pass extra parameters to an activity that
 * reads barcodes.
 */
public class ScannerActivity extends Activity implements View.OnClickListener {

    // use a compound button so either checkbox or switch widgets work
    private String statusMessage;
    //barcode value to use
    private String barcodeValue;

    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        findViewById(R.id.read_barcode).setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.read_barcode) {
            // launch barcode activity.
            Intent intent = new Intent(this, BarcodeCaptureActivity.class);
            startActivityForResult(intent, RC_BARCODE_CAPTURE);
        }
        else if (v.getId() == R.id.searchCosmetic) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
        }
        else if(v.getId() == R.id.logOut){
            FirebaseAuth.getInstance().signOut();
            Toast.makeText(ScannerActivity.this, " Logged out succesfully", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(), LogInActivity.class));
            finish();
        }
        else if(v.getId() == R.id.exit){
            finishAffinity();
            System.exit(0);
        }
        else if(v.getId() == R.id.myAccount){
            Intent intent = new Intent(this, AccountActivity.class);
            startActivity(intent);
        }
    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onBackPressed()
    {
        //do nothing when try go back
    }

    /**
     * Called when an activity you launched exits, giving you the requestCode
     * you started it with, the resultCode it returned, and any additional
     * data from it.  The <var>resultCode</var> will be
     * {@link #RESULT_CANCELED} if the activity explicitly returned that,
     * didn't return any result, or crashed during its operation.
     * <p/>
     * <p>You will receive this call immediately before onResume() when your
     * activity is re-starting.
     * <p/>
     *
     * @param requestCode The integer request code originally supplied to
     *                    startActivityForResult(), allowing you to identify who this
     *                    result came from.
     * @param resultCode  The integer result code returned by the child activity
     *                    through its setResult().
     * @param data        An Intent, which can return result data to the caller
     *                    (various data can be attached to Intent "extras").
     * @see #startActivityForResult
     * @see #createPendingResult
     * @see #setResult(int)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final Long id;
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    //statusMessage.setText(R.string.barcode_success);
                    id = Long.parseLong(barcode.displayValue);
                    AfterScan.passData(id, this);

                } else {
                    //statusMessage.setText(R.string.barcode_failure);
                    Log.d(TAG, "No read barcode");
                }
            } else {
                //statusMessage.setText(String.format(getString(R.string.barcode_error),
                       // CommonStatusCodes.getStatusCodeString(resultCode)));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
