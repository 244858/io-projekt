package edu.io;

import android.os.Bundle;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.google.firebase.firestore.FirebaseFirestore;


public class IngredientDetailActivity extends AppCompatActivity {


    TextView detailTextView;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredient_detail);

        detailTextView = findViewById(R.id.detail);
        Bundle extras = getIntent().getExtras();
        String detail = extras.getString("detail");
        detailTextView.setText(detail);
    }
}
