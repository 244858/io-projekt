package edu.io.addCosmetic;


public class IngredientItem {
    private boolean checked;
    private String ItemString;

    public void setChecked(boolean checked) {
        this.checked = checked;
    }


    public String getItemString() {
        return ItemString;
    }

    public void setItemString(String itemString) {
        ItemString = itemString;
    }

    IngredientItem(String t, boolean b){
        ItemString = t;
        checked = b;
    }

    public boolean isChecked(){
        return checked;
    }
}
