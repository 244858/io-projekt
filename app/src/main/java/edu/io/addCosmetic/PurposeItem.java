package edu.io.addCosmetic;


public class PurposeItem {
    private boolean checked;
    private String name;

    void setChecked(boolean checked) {
        this.checked = checked;
    }


    String getItemString() {
        return name;
    }

    public void setItemString(String itemString) {
        name = itemString;
    }

    PurposeItem(String name, boolean checked){
        this.name = name;
        this.checked = checked;
    }

    boolean isChecked(){
        return checked;
    }
}
