package edu.io.addCosmetic;

import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;

import java.util.List;
import java.util.Objects;

import edu.io.AfterScan;
import edu.io.cosmeticInfo.CosmeticInfoActivity;
import edu.io.R;
import edu.io.persistance.objects.Cosmetic;
import edu.io.persistance.objects.Ingredient;

public class AddToDbActivity extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    EditText cosmeticName;
    EditText cosmeticProducer;
    TextView cosmeticHowToUse;
    IngredientListAdapter ingredientListAdapter;
    ListView purposesListView;
    PurposeListAdapter purposeListAdapter;
    AutoCompleteTextView cosmeticIngredient;
    ListView ingredientsListView;

    List<PurposeItem> purposeItems;
    List<Ingredient> addedIngredients = new ArrayList<>();
    private  List<Ingredient> ingredients =  new ArrayList<>();

    Cosmetic cosmetic;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cosmetic);

        cosmeticName = findViewById(R.id.cosmeticName);
        cosmeticProducer = findViewById(R.id.cosmeticProducer);
        cosmeticHowToUse = findViewById(R.id.howToUse);
        cosmeticIngredient = findViewById(R.id.cosmeticIngredient);
        purposesListView = findViewById(R.id.purpose_listView);
        ingredientsListView = findViewById(R.id.ingredients_listView);

        firebaseAuth = FirebaseAuth.getInstance();

        db.collection("ingredients").get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    Ingredient ingredient = document.toObject(Ingredient.class);
                    ingredients.add(ingredient);
                }
                ArrayAdapter<Ingredient> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, ingredients);
                cosmeticIngredient.setAdapter(adapter);
            }
        });

        Bundle extras = getIntent().getExtras();
        cosmetic = (Cosmetic) extras.get(AfterScan.COSMETIC);

        initItems();
        purposeListAdapter = new PurposeListAdapter(this, purposeItems);
        purposesListView.setAdapter(purposeListAdapter);

        ingredientListAdapter = new IngredientListAdapter(this, addedIngredients, ingredientsListView);
        ingredientsListView.setAdapter(ingredientListAdapter);
    }


    public void addCosmetic(View view) {

        String name = cosmeticName.getText().toString();
        String producer = cosmeticProducer.getText().toString();
        String howToUse = cosmeticHowToUse.getText().toString();

        ArrayList<String> purposes = new ArrayList<>();

        for(PurposeItem purpose : purposeItems){
            if(purpose.isChecked()){
                purposes.add(purpose.getItemString());
            }
        }

        cosmetic.setName(name);
        cosmetic.setProducer(producer);
        cosmetic.setIngredients(addedIngredients);
        cosmetic.setPurpose(purposes);
        cosmetic.setHowToUse(howToUse);

        addCosmetic(cosmetic);
    }

    private void addCosmetic(final Cosmetic cosmetic) {
        String email = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getEmail();
        CollectionReference colRef = db.collection("customers");

        CollectionReference mColRef = db.collection(AfterScan.COSMETICS_COLLECTION);
        mColRef.document(String.valueOf(cosmetic.getId())).set(cosmetic)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Log.d("***COSMETIC ADDED***", "Document has been saved");
                        Toast.makeText(getApplicationContext(), "Cosmetic added to database!", Toast.LENGTH_SHORT).show();

                        colRef.document(email).get().addOnCompleteListener(task2 -> {
                            if (task.isSuccessful()) {
                                DocumentSnapshot document = task2.getResult();
                                if (document.exists()) {
                                    Long score = (Long) document.get("score");
                                    score += 5;
                                    document.getReference().update("score", score);
                                } else {
                                    Log.d("***NO DATA***", "No such document");
                                }
                            } else {
                                Log.d("***FAILED***", "get failed with ", task.getException());
                                ingredients = null;
                            }
                        });

                        Intent intent = new Intent(getApplicationContext(), CosmeticInfoActivity.class);
                        intent.putExtra("COSMETIC", cosmetic);
                        startActivity(intent);
                    }
                }).addOnFailureListener(e -> Log.w("***FAILED ADD***", "Document was not saved", e));
    }

    private void initItems(){
        purposeItems = new ArrayList<>();

        TypedArray arrayText = getResources().obtainTypedArray(R.array.restext);

        for(int i = 0; i < arrayText.length() ; i++){
            String s = arrayText.getString(i);
            PurposeItem item = new PurposeItem(s, false);
            purposeItems.add(item);
        }
        arrayText.recycle();
    }

    public void addIngredient(View view){
        if(cosmeticIngredient.getText().length() > 0) {
            Ingredient ingredient = new Ingredient();
            for(Ingredient ing : ingredients){
                if (cosmeticIngredient.getText().toString().equals(ing.getName())){
                    ingredient = ing;
                    break;
                }
                ingredient = new Ingredient(cosmeticIngredient.getText().toString(), "unknown", "detail");
            }
            cosmeticIngredient.setText("");
            ingredientListAdapter.add(ingredient);
            ingredientListAdapter.notifyDataSetChanged();
        }
    }

    //hide keyboard when  touched outside editText
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyboard(this);
        }
        return super.dispatchTouchEvent(ev);
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }
}
