package edu.io.addCosmetic;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import edu.io.R;

public class PurposeListAdapter extends BaseAdapter {

    private Context context;
    private List<PurposeItem> list;

    PurposeListAdapter(Context context, List<PurposeItem> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private boolean isChecked(int position) {
        return list.get(position).isChecked();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        // reuse views
        ViewHolder viewHolder = new ViewHolder();
        if (rowView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            rowView = inflater.inflate(R.layout.purpose_list_row, null);

            viewHolder.checkBox = (CheckBox) rowView.findViewById(R.id.rowCheckBox);
            viewHolder.text = (TextView) rowView.findViewById(R.id.rowTextView);
            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }
        viewHolder.checkBox.setChecked(list.get(position).isChecked());

        final String itemStr = list.get(position).getItemString();
        viewHolder.text.setText(itemStr);

        viewHolder.checkBox.setTag(position);


        viewHolder.checkBox.setOnClickListener(view -> {
            boolean newState = !list.get(position).isChecked();
            list.get(position).setChecked(newState);
        });

        viewHolder.checkBox.setChecked(isChecked(position));

        return rowView;
    }
}

class ViewHolder {
    CheckBox checkBox;
    TextView text;
}

