package edu.io.addCosmetic;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import edu.io.R;
import edu.io.persistance.objects.Ingredient;

public class IngredientListAdapter extends BaseAdapter {

    private Context context;
    private List<Ingredient> ingredients;
    private ListView ingredientsListView;

    public void add(Ingredient item){
        ingredients.add(item);
    }

    public void remove(int position){
        ingredients.remove(position);
    }

    IngredientListAdapter(Context context, List<Ingredient> ingredients, ListView ingredientsListView) {
        this.context = context;
        this.ingredients = ingredients;
        this.ingredientsListView = ingredientsListView;
    }

    @Override
    public int getCount() {
        return ingredients.size();
    }

    @Override
    public Object getItem(int position) {
        return ingredients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // reuse views
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View rowView = inflater.inflate(R.layout.ingredient_list_row, null);
        TextView nameTextView = (TextView) rowView.findViewById(R.id.rowTextView);
        Button button = (Button) rowView.findViewById(R.id.rowButton);

        nameTextView.setText(ingredients.get(position).getName());
        //button.setText(String.valueOf(position));

        button.setOnClickListener(view -> {
            remove(position);
            notifyDataSetChanged();
        });

        return rowView;
    }
}

class IngredientViewHolder {
    TextView text;
    Button button;
}
