package edu.io;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;
import java.util.List;
import edu.io.cosmeticInfo.CosmeticInfoActivity;
import edu.io.cosmeticInfo.IngredientInfoAdapter;
import edu.io.persistance.objects.Cosmetic;
import edu.io.persistance.objects.Ingredient;

public class IngredientsInfoActivity extends AppCompatActivity {

    final static String INGREDIENTS_COLLECTION = "ingredients";

    List<Ingredient> cosmeticHealthyIngredients = new ArrayList<>();
    List<Ingredient> cosmeticHarmfulIngredients = new ArrayList<>();
    List<Ingredient> ingredients;

    ListView harmfulListView;
    ListView healthyListView;
    Cosmetic cosmetic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredients_info);

        harmfulListView = findViewById(R.id.harmfulIngredientsList);
        healthyListView = findViewById(R.id.healthyIngredientsList);

        Bundle extras = getIntent().getExtras();
        cosmetic = (Cosmetic) extras.get("COSMETIC");

        ingredients = cosmetic.getIngredients();

        for(Ingredient ingredient : ingredients){
            if(ingredient.getHarmfulness().equals("no")){
                cosmeticHealthyIngredients.add(ingredient);
            }else if(ingredient.getHarmfulness().equals("yes")){
                cosmeticHarmfulIngredients.add(ingredient);
            }
        }

        IngredientInfoAdapter healthyAdapter = new IngredientInfoAdapter(this, cosmeticHealthyIngredients, healthyListView, true);
        IngredientInfoAdapter harmfulAdapter = new IngredientInfoAdapter(this, cosmeticHarmfulIngredients, harmfulListView, false);

        healthyListView.setAdapter(healthyAdapter);
        harmfulListView.setAdapter(harmfulAdapter);

        harmfulListView.setOnItemClickListener((parent, view, position, id) -> {
            Ingredient ingredient = cosmeticHarmfulIngredients.get(position);
            Intent intent = new Intent(this, IngredientDetailActivity.class);
            intent.putExtra("detail", ingredient.getDetail());
            startActivity(intent);
        });

        healthyListView.setOnItemClickListener((parent, view, position, id) -> {
            Ingredient ingredient = cosmeticHealthyIngredients.get(position);
            Intent intent = new Intent(this, IngredientDetailActivity.class);
            intent.putExtra("detail", ingredient.getDetail());
            startActivity(intent);
        });
    }

    public void onClick (View view){
        if (view.getId() == R.id.backBtnFloat2) {
            Intent intent = new Intent(this, CosmeticInfoActivity.class);
            intent.putExtra("COSMETIC", cosmetic);
            startActivity(intent);
        }
    }
}
