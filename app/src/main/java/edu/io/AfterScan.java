package edu.io;

import android.content.Intent;
import android.util.Log;
import androidx.annotation.NonNull;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import edu.io.addCosmetic.AddToDbActivity;
import edu.io.cosmeticInfo.CosmeticInfoActivity;
import edu.io.persistance.objects.Cosmetic;
import edu.io.scanner.ScannerActivity;

public class AfterScan {

    public final static String COSMETICS_COLLECTION = "cosmetics";
    public final static String COSMETIC = "COSMETIC";

    public static void passData(final Long id, final ScannerActivity scannerActivity) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        final Cosmetic[] cosmetic = new Cosmetic[1];

        DocumentReference docRef = db.document(COSMETICS_COLLECTION + "/" + id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("***DATA***", "DocumentSnapshot data: " + document.getData());
                        cosmetic[0] = document.toObject(Cosmetic.class);
                        Intent intent = new Intent(scannerActivity, CosmeticInfoActivity.class);
                        intent.putExtra(COSMETIC, cosmetic[0]);
                        scannerActivity.startActivity(intent);
                    } else {
                        Log.d("***NO DATA***", "No such document");
                        Intent intent = new Intent(scannerActivity, AddToDbActivity.class);
                        cosmetic[0] = new Cosmetic();
                        cosmetic[0].setId(id);
                        intent.putExtra(COSMETIC, cosmetic[0]);
                        scannerActivity.startActivity(intent);
                    }
                } else {
                    Log.d("***FAILED***", "get failed with ", task.getException());
                    cosmetic[0] = null;
                }
            }
        });
    }
}
