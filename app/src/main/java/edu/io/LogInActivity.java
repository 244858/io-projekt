package edu.io;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import edu.io.scanner.ScannerActivity;

public class LogInActivity extends AppCompatActivity {

    EditText userPassword, userEmail;
    Button loginBtn;
    FirebaseAuth firebaseAuth;
    ProgressBar progressBar;
    TextView createAccountTxt;
    TextView forgetPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userEmail = findViewById(R.id.email);
        userPassword = findViewById(R.id.password);
        progressBar = findViewById(R.id.progressBar);
        firebaseAuth = FirebaseAuth.getInstance();
        loginBtn = findViewById(R.id.loginBtn);
        createAccountTxt = findViewById(R.id.registerText);
        forgetPass = findViewById(R.id.forgetPass);

        loginBtn.setOnClickListener(view -> {
            String email = userEmail.getText().toString().trim();
            String password = userPassword.getText().toString().trim();

            if (TextUtils.isEmpty(email)) {
                userEmail.setError("Email is required.");
                return;
            }
            if (TextUtils.isEmpty(password)) {
                userPassword.setError("Password is required.");
                return;
            }

            if (password.length() < 6) {
                userPassword.setError("Password must be more than  or equals 6 characters. ");
                return;
            }

            progressBar.setVisibility(View.VISIBLE);

            //authenitication of user

            firebaseAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                if (firebaseAuth.getCurrentUser().isEmailVerified()) {
                                    Toast.makeText(LogInActivity.this, " Logged in succesfully", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getApplicationContext(), ScannerActivity.class));

                                } else {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(LogInActivity.this, "Please verify your email", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(LogInActivity.this, "Error !" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        });
    }

    //hide keyboard when  touched outside editText
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyboard(this);
        }
        return super.dispatchTouchEvent(ev);
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    public void onClickForget(View view) {
        String email = userEmail.getText().toString();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        if (email.contains("@") && email.length() >= 5) {
            mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(LogInActivity.this, "Password reset link send to your email", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(LogInActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });

        } else
            Toast.makeText(LogInActivity.this, "Type correct mail to get password reset.", Toast.LENGTH_LONG).show();
    }

}
