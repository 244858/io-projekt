package edu.io;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import java.util.ArrayList;
import java.util.List;
import edu.io.cosmeticInfo.CosmeticInfoActivity;
import edu.io.persistance.objects.Cosmetic;
import edu.io.scanner.ScannerActivity;

public class SearchActivity extends AppCompatActivity {

    AutoCompleteTextView cosmeticTextView;
    List<Cosmetic> cosmeticList = new ArrayList<>();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    Cosmetic chosenCosmetic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        cosmeticTextView = findViewById(R.id.cosmeticsTextView);

        db.collection("cosmetics").get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    Cosmetic cosmetic = document.toObject(Cosmetic.class);
                    cosmeticList.add(cosmetic);
                }
            }
        });

        ArrayAdapter<Cosmetic> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, cosmeticList);
        cosmeticTextView.setAdapter(adapter);

        cosmeticTextView.setOnItemClickListener((parent, view, position, id) -> {
            Cosmetic selectedCosmetic = (Cosmetic) parent.getAdapter().getItem(position);
            chosenCosmetic = selectedCosmetic;
        });
    }

    public void onClick(View v) {
        if (v.getId() == R.id.backFloatBtn3) {
            Intent intent = new Intent(this, ScannerActivity.class);
            startActivity(intent);
        }
        else if(v.getId() == R.id.choose){
            Intent intent = new Intent(this, CosmeticInfoActivity.class);
            intent.putExtra("COSMETIC", chosenCosmetic);
            startActivity(intent);
        }
    }

    //hide keyboard when  touched outside editText
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyboard(this);
        }
        return super.dispatchTouchEvent(ev);
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }
}
