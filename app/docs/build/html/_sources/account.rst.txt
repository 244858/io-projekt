My account
======================

In the "My account" tab you can see information about the points you have collected. Each cosmetic added to the database is 5 points for your account.
You can also view cosmetics on your shelf, and after clicking on them you can get to the detailed information view.

.. image:: img/account.jpg
           :width: 300
           :align: center


