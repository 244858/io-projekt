.. Cosmetic Advisor documentation master file, created by
   sphinx-quickstart on Sat Jun  6 11:59:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cosmetic Advisor's documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about
   registration
   login
   add
   scan
   account
   searchs
   database
