Scan cosmetic
======================

If you want to know the information about the cosmetic, scan its bar code. You will see a screen with detailed information.

You can read detailed information about individual ingredients and see what the cosmetic is for.

You can also add a cosmetic to your shelf, thanks to which you will have easy and quick access to it.

.. image:: img/scanExist.gif
           :align: center


How to use
-------------------------------


Shows detailed information on the use of the cosmetic.

.. image:: img/howUse.jpg
           :width: 300
           :align: center



Ingredients information
-------------------------------

It shows individual components of the cosmetic with a division into harmful and positively affecting the skin.


.. image:: img/ingridients.jpg
           :width: 300
           :align: center



Detail ingredients information
-------------------------------



When we click on the selected ingredient in the list of individual ingredients in a given cosmetic, we will be shown the justification for its presence in a given category (harmful, positive).

.. image:: img/detail.jpg
           :width: 300
           :align: center
