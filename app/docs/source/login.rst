Log in
======================

To log in enter the data with which you registered.

.. image:: img/login.gif
           :align: center

.. note::

    Remember to verify your email address first.

Restore password
--------------------

If you have forgotten your password or want to change it, enter your email address in the login screen and touch the password recovery button.
You will receive an e-mail that will allow it.