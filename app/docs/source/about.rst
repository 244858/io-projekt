About
======================

Cosmetic Advisor is an application supporting modern and conscious users of cosmetics.
It allows you to add new cosmetics to the database and scan bar codes already in the existing database.

It offers the user detailed information on the composition of cosmetics and indicates which substances are potentially harmful.
The user can read what the cosmetic is for. It also has the option of adding it to your shelf to have quick and easy access to them.

Use case diagram
----------------------------


.. uml::

    left to right direction
    skinparam packageStyle rectangle
    actor client
    actor database
    rectangle CosmeticAdvisor {
      client -- (log in)
      client -- (register)
      client -- (scan a cosmetic bar code)
      client -- (search cosmetic by name)
      client -- (show cosmetic information)
      client -- (show information about ingredients)
      client -- (add cosmetic to client's profile)
      (log in) -- database
      (register) -- database
      (show cosmetic information) -- database
      (search cosmetic by name) -- database
      (scan a cosmetic bar code) -- database
      (add cosmetic to client's profile) -- database
      (show information about ingredients) -- database
    }

Requirements
--------------------

To run the application you need a smartphone with Android version 7.0 or higher, permission to access the device's camera and access to the Internet.




