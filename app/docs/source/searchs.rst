Search cosmetics
======================

In the "Search" tab you can search cosmetics from the database. Just start typing the name and you will immediately see a list of matching suggestions from which you can choose.

.. image:: img/search.gif
           :align: center
