New Cosmetic
======================

Scan new cosmetic barcode
-------------------------------

If you want to enter a cosmetic that is not in the database, use the "read barcode" button in the menu and scan the cosmetic bar code.

.. image:: img/add1.gif
           :align: center

Adding new cosmetic info
------------------------------

Once you have scanned the barcode, fill in all the data.

.. image:: img/add2.gif
          :align: center

.. note::

    Remember to complete the data in accordance with reality and as accurately as possible.
    After adding the cosmetic to the base, you get +5 points to your account.
