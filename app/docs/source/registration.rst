Registration
======================

Create account
--------------------

To create an account choose the appropriate option in the main view. Enter your email, name and password. Password must be at least 6 characters.
It is important to enter the email address to which we have access because it will be necessary to verify it.

.. image:: img/register.gif
           :align: center

Verify email adress
-----------------------

After registration, you will receive a message with a link to the confirmation email. Use it to complete the registration.

Once you do this, you can login.