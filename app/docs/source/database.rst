Database
======================

Collections
-------------------------------


Our application uses the Firebase database. In order for our application to work properly, we need three collections: cosmetics, customers and ingredients.

.. image:: img/database1.png
           :width: 300
           :align: center



Cosmetics
-------------------------------

This is a collection that stores all information about added cosmetics. The identifier of a given cosmetic is the number read from the bar code.


.. image:: img/database2.png
           :width: 600
           :align: center


Customers
-------------------------------


This is a collection that stores information about registered users and cosmetics added to their shelves, i.e. those they currently use.

.. image:: img/databse3.png
           :width: 600
           :align: center


Ingredients
-------------------------------

This is a collection that stores detailed information about a particular ingredient used in the production of cosmetics. This information also includes information about the harmful effects or positive effects on the skin and the justification for these characteristics.

.. image:: img/database.png
           :width: 600
           :align: center